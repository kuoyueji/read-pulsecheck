﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Item Name="我的电脑" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">我的电脑/VI服务器</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">我的电脑/VI服务器</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="pulseCheck.vi" Type="VI" URL="../pulseCheck.vi"/>
		<Item Name="ReadPulseCheckTest.vi" Type="VI" URL="../ReadPulseCheckTest.vi"/>
		<Item Name="依赖关系" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
			</Item>
			<Item Name="gaussian_fit.vi" Type="VI" URL="../gaussian_fit.vi"/>
		</Item>
		<Item Name="程序生成规范" Type="Build">
			<Item Name="APERead_EXE" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{7D0BDEA6-8BC5-4509-B0A7-71D552C4048D}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A5537BA9-36AC-45B0-A524-AA126BAD1B01}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{4697021C-0825-43C3-AAD5-6BFB5FD22C28}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">APERead_EXE</Property>
				<Property Name="Bld_defaultLanguage" Type="Str">ChineseS</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Builds/EXE</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{029EAF25-29C8-4307-A90F-3D04113A8C70}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">APE Read.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../Builds/EXE/APE Read.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">支持目录</Property>
				<Property Name="Destination[1].path" Type="Path">../Builds/EXE/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_actXinfo_enumCLSID[0]" Type="Str">{E7BCF117-05B8-4225-98D4-1592346D95A5}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[1]" Type="Str">{D9E92EF1-D67C-4048-AA14-4309987EA3C1}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[10]" Type="Str">{9C771C7B-A5B2-4D16-A42D-EF87D79A35A6}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[11]" Type="Str">{54BCAE44-AA2A-4549-97D6-A8CF422CC973}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[12]" Type="Str">{6E8B053E-7FD9-4905-82BF-ABE544EEBD95}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[13]" Type="Str">{722FBD14-26CF-450D-8181-BBC5285BB537}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[14]" Type="Str">{EC9A7FB8-2FFF-4491-98B7-6C779C59C87B}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[15]" Type="Str">{CCECE2E3-BFE6-4730-9688-CE4DECC1330B}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[16]" Type="Str">{63AC26B9-7264-4686-845A-CCDCCCF71CC7}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[2]" Type="Str">{A1543F7E-35DC-4719-8D69-18885BEA37C9}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[3]" Type="Str">{23B46662-34A7-4A42-A1AE-ECD11DDAF6F5}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[4]" Type="Str">{04E09F06-932C-41F4-B8DF-CA4F3A8813A6}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[5]" Type="Str">{8325E0F1-4F11-4E87-BC02-4CCE38F3896B}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[6]" Type="Str">{C94100B1-BA4C-4395-82B5-9DD61F74988A}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[7]" Type="Str">{5E1FEC15-8C77-416E-BEDB-ABCF2EA7846E}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[8]" Type="Str">{04A8B209-C2D5-4853-B6B6-D8C6F5981A81}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[9]" Type="Str">{5CD81D8F-1054-4AF5-AF18-B9E146FBECE7}</Property>
				<Property Name="Exe_actXinfo_enumCLSIDsCount" Type="Int">17</Property>
				<Property Name="Exe_actXinfo_majorVersion" Type="Int">5</Property>
				<Property Name="Exe_actXinfo_minorVersion" Type="Int">5</Property>
				<Property Name="Exe_actXinfo_objCLSID[0]" Type="Str">{3DBFCAC1-572C-44B5-A171-C706E1B0A0F9}</Property>
				<Property Name="Exe_actXinfo_objCLSID[1]" Type="Str">{7B598BA1-2CCF-43EF-96F3-4E7967467012}</Property>
				<Property Name="Exe_actXinfo_objCLSID[10]" Type="Str">{347DFB87-AF6B-4B56-B9DF-B52B106963A8}</Property>
				<Property Name="Exe_actXinfo_objCLSID[11]" Type="Str">{F0061EB9-1839-4EAF-80FF-4558C96262C2}</Property>
				<Property Name="Exe_actXinfo_objCLSID[12]" Type="Str">{A5BA8263-2720-473D-B998-586F866AC805}</Property>
				<Property Name="Exe_actXinfo_objCLSID[13]" Type="Str">{5A0B18C7-9586-4AB9-AA03-1ECB78592B0A}</Property>
				<Property Name="Exe_actXinfo_objCLSID[2]" Type="Str">{D98F72E9-0E6D-4111-A057-03CC3A981F22}</Property>
				<Property Name="Exe_actXinfo_objCLSID[3]" Type="Str">{0A8C45A0-9F8B-44C2-8FE3-9ED96BC471D0}</Property>
				<Property Name="Exe_actXinfo_objCLSID[4]" Type="Str">{6E5A96EC-101A-4F02-9D54-6B7C01151922}</Property>
				<Property Name="Exe_actXinfo_objCLSID[5]" Type="Str">{595E08A6-5449-413F-B636-3D3414C51253}</Property>
				<Property Name="Exe_actXinfo_objCLSID[6]" Type="Str">{60F176B2-EAFA-469E-9550-F9915E7AC15E}</Property>
				<Property Name="Exe_actXinfo_objCLSID[7]" Type="Str">{569539AA-0BCB-4D81-923B-191AEBFB555F}</Property>
				<Property Name="Exe_actXinfo_objCLSID[8]" Type="Str">{E8581798-DEC9-4A02-B944-F0AB3D4D40D6}</Property>
				<Property Name="Exe_actXinfo_objCLSID[9]" Type="Str">{D8B51B2A-8F85-438C-BD40-EE8787A73A34}</Property>
				<Property Name="Exe_actXinfo_objCLSIDsCount" Type="Int">14</Property>
				<Property Name="Exe_actXinfo_progIDPrefix" Type="Str">APERead</Property>
				<Property Name="Exe_actXServerName" Type="Str">APERead</Property>
				<Property Name="Exe_actXServerNameGUID" Type="Str">{77EC8A3C-10D9-4493-BADA-2F1DA1E57B94}</Property>
				<Property Name="Source[0].itemID" Type="Str">{6F8E57A0-A263-49B1-962A-13D2282F7AFC}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/我的电脑/ReadPulseCheckTest.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">SIOM</Property>
				<Property Name="TgtF_fileDescription" Type="Str">APERead_EXE</Property>
				<Property Name="TgtF_internalName" Type="Str">APERead_EXE</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">版权 2022 </Property>
				<Property Name="TgtF_productName" Type="Str">APERead_EXE</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{80D0C3A9-DA38-459A-B6E1-F0967FDFD368}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">APE Read.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="我的安装程序" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">PulseCheck</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{BFAA5383-8FFE-4327-8591-1DB9C7F7F33A}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[0].productID" Type="Str">{5270CDA8-6512-4F0E-8BAE-1CE3ECC2FDFD}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI-Serial运行引擎 17.5</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{A0A2CF10-0C02-41DF-AC3F-1EBA24038C19}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-VISA运行引擎 18.0</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{FEB8324C-1913-4673-9B2E-89F3D074E0BD}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW运行引擎 2018</Property>
				<Property Name="DistPart[2].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[0].productName" Type="Str">NI LabVIEW运行引擎 2018非英语语言支持</Property>
				<Property Name="DistPart[2].SoftDep[0].upgradeCode" Type="Str">{3C68D03D-EF38-41B5-9977-E27520759DD6}</Property>
				<Property Name="DistPart[2].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[1].productName" Type="Str">NI ActiveX容器</Property>
				<Property Name="DistPart[2].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[2].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[10].productName" Type="Str">NI mDNS Responder 17.0</Property>
				<Property Name="DistPart[2].SoftDep[10].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[2].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[11].productName" Type="Str">NI Deployment Framework 2018</Property>
				<Property Name="DistPart[2].SoftDep[11].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[2].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[12].productName" Type="Str">NI错误报告 2018</Property>
				<Property Name="DistPart[2].SoftDep[12].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[2].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[2].productName" Type="Str">数学核心库 2017</Property>
				<Property Name="DistPart[2].SoftDep[2].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[2].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[3].productName" Type="Str">数学核心库 2018</Property>
				<Property Name="DistPart[2].SoftDep[3].upgradeCode" Type="Str">{33A780B9-8BDE-4A3A-9672-24778EFBEFC4}</Property>
				<Property Name="DistPart[2].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[4].productName" Type="Str">NI Logos 18.0</Property>
				<Property Name="DistPart[2].SoftDep[4].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[2].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[5].productName" Type="Str">NI TDM Streaming 18.0</Property>
				<Property Name="DistPart[2].SoftDep[5].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[2].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[6].productName" Type="Str">NI LabVIEW Web服务器 2018</Property>
				<Property Name="DistPart[2].SoftDep[6].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[2].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[7].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2018</Property>
				<Property Name="DistPart[2].SoftDep[7].upgradeCode" Type="Str">{EF4708F6-5A34-4DBA-B12B-79CC2004E20B}</Property>
				<Property Name="DistPart[2].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[8].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[2].SoftDep[8].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[2].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[2].SoftDep[9].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[2].SoftDep[9].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[2].SoftDepCount" Type="Int">13</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{3B195EBF-4A09-46E6-8EAD-931568C1344C}</Property>
				<Property Name="DistPartCount" Type="Int">3</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../read-pulsecheck/Builds/Setup</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">我的安装程序</Property>
				<Property Name="INST_defaultDir" Type="Str">{BFAA5383-8FFE-4327-8591-1DB9C7F7F33A}</Property>
				<Property Name="INST_language" Type="Int">2052</Property>
				<Property Name="INST_productName" Type="Str">PulseCheck</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.1</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">18008007</Property>
				<Property Name="MSI_arpCompany" Type="Str">SIOM</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{843B65A0-29F8-41B4-96A5-17F6001427DF}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{9174F3EE-CB0D-4A61-A4FA-8011CD215A86}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{BFAA5383-8FFE-4327-8591-1DB9C7F7F33A}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{BFAA5383-8FFE-4327-8591-1DB9C7F7F33A}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">APE Read.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">1</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">APE Read</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str"></Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{80D0C3A9-DA38-459A-B6E1-F0967FDFD368}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">APERead_EXE</Property>
				<Property Name="Source[0].tag" Type="Ref">/我的电脑/程序生成规范/APERead_EXE</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
